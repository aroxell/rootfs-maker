#!/bin/bash

# Save image and all related artifacts
if [ -f "${CI_PROJECT_DIR}/date.txt" ]; then
  rootfs_date="$(cat "${CI_PROJECT_DIR}/date.txt")"
else
  echo "No date.txt found. Using today's date."
  rootfs_date="$(date +%Y%m%d)"
fi

if [ -f "${CI_PROJECT_DIR}/pub-dest.txt" ]; then
  pub_dest="$(cat "${CI_PROJECT_DIR}/pub-dest.txt")"
fi

# Install awscli and dir2bundle binaries
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip -q awscliv2.zip
./aws/install --update -i ~/bin/aws-cli -b ~/bin
git clone https://gitlab.com/Linaro/lkft/rootfs/dir2bundle.git ~/dir2bundle

if [ "${IMAGE}" = "${IMAGE/image}" ]; then
  # Building packages

  pwd
  ls -ls .
  ls -ls build
  ls -ls build/tmp*/
  ls -ls build/tmp*/deploy/
  temp_dir="$(find build -maxdepth 1 -name "tmp*" -type d)"
  deploy_dir="$(find "${temp_dir}" -maxdepth 1 -name deploy -type d)"
  ls -ls "${deploy_dir}"

#  # Create a list of packages to collect
#  for recipe in $IMAGE; do
#    echo "${recipe}"
#    ionice -c3 nice -n19 bash loeb -s -c /data/oe-caches bitbake -e "${recipe}" | grep ^PACKAGES= >> "${CI_PROJECT_DIR}/list.txt"
#  done
#  echo "list.txt:"
#  cat "${CI_PROJECT_DIR}/list.txt"

#  pwd
#  ls -ls .
#  ls -ls build
#  ls -ls build/tmp*/
#  ls -ls build/tmp*/deploy/
#  temp_dir="$(find build -maxdepth 1 -name "tmp*" -type d)"
#  deploy_dir="$(find "${temp_dir}" -maxdepth 1 -name deploy -type d)"
#  ls -ls "${deploy_dir}"

  echo "More debug"
  find . -type d -name "deploy"
  #find . -type f -name "*.ipk"

  # Gather all packages in a single, closer directory
  dest_dir="packages/${rootfs_date}/${MACHINE}"
  mkdir -p "${dest_dir}"
  for package in $(sed -e 's#PACKAGES=##g' -e 's#"##g' "${CI_PROJECT_DIR}/list.txt" | tr '\n' ' '); do
    find "${deploy_dir}" -name "${package}_*.*"
  done | xargs cp -p -t "${dest_dir}/"

  # Copy to AWS S3 bucket
  if [ -z "${pub_dest}" ]; then
    branch_dir=${MANIFEST_BRANCH/\//_}
    branch_dir=${branch_dir,,}
    s3_dest="s3://storage.lkft.org/packages.new/oe-${branch_dir}/${rootfs_date}/${MACHINE}"
  else
    s3_dest="${pub_dest}/${MACHINE}"
  fi
  ~/bin/aws s3 sync --acl public-read \
    "packages/${rootfs_date}/${MACHINE}/" \
    "${s3_dest}/"
else
  # Building complete image

  dest_dir="images/${rootfs_date}/${MACHINE}"
  mkdir -p "${dest_dir}"
  rsync -axv build/tmp*/deploy/images/"${MACHINE}"/ "${dest_dir}/"

  # Create bundle.json
  # shellcheck disable=SC2164
  pushd "images/${rootfs_date}/${MACHINE}/"
  # shellcheck disable=SC2012
  (ls | ~/dir2bundle/dir2bundle "${MACHINE}" > bundle.json) ||:
  # shellcheck disable=SC2164
  popd

  # Copy to AWS S3 bucket
  if [ -z "${pub_dest}" ]; then
    branch_dir=${MANIFEST_BRANCH/\//_}
    branch_dir=${branch_dir,,}
    s3_dest="s3://storage.lkft.org/rootfs/oe-${branch_dir}/${rootfs_date}/${MACHINE}"
  else
    s3_dest="${pub_dest}/${MACHINE}"
  fi
  ~/bin/aws s3 sync --acl public-read \
    "images/${rootfs_date}/${MACHINE}/" \
    "${s3_dest}/"
fi
